#!/bin/bash
#
# biglinux-welcome
#
# Painel de boas vindas para o BigLinux
# Criado por eltonff e Danylo (LordDan) iniciado em 17/12/2020
# Tecnologias usadas: html, css, javascript (jQuery)
# Renderizado pelo BigBashView ;)
# Theme: Cerulean (bootswatch)

mkdir -p ~/.config/autostart

if [ -e ~/.config/autostart/biglinux-welcome.desktop ];then
    rm ~/.config/autostart/biglinux-welcome.desktop
else
    echo "[Desktop Entry]
Hidden=true" > ~/.config/autostart/biglinux-welcome.desktop
fi
exit