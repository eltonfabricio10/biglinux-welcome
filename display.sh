#!/bin/bash
#
# biglinux-welcome
#
# Painel de boas vindas para o BigLinux
# Criado por eltonff e Danylo (LordDan) iniciado em 17/12/2020
# Tecnologias usadas: html, css, javascript (jQuery)
# Renderizado pelo BigBashView ;)
# Theme: Cerulean (bootswatch)

kwriteconfig5 --file ~/.config/kscreenlockerrc --group Daemon --key Autolock false
kwriteconfig5 --file ~/.config/kscreenlockerrc --group Daemon --key LockOnResume false

kwriteconfig5 --file ~/.config/powermanagementprofilesrc --group AC --group DPMSControl --key idleTime --delete
kwriteconfig5 --file ~/.config/powermanagementprofilesrc --group Battery --group DPMSControl --key idleTime --delete

kwriteconfig5 --file ~/.config/powermanagementprofilesrc --group AC --group DimDisplay --key idleTime --delete
kwriteconfig5 --file ~/.config/powermanagementprofilesrc --group Battery --group DimDisplay --key idleTime --delete

kwriteconfig5 --file ~/.config/powermanagementprofilesrc --group AC --group BrightnessControl --key value 100
kwriteconfig5 --file ~/.config/powermanagementprofilesrc --group Battery --group BrightnessControl --key value 100

exit