#!/bin/bash
#
# biglinux-welcome
#
# Painel de boas vindas para o BigLinux
# Criado por eltonff e Danylo (LordDan) iniciado em 17/12/2020
# Tecnologias usadas: html, css, javascript (jQuery)
# Renderizado pelo BigBashView ;)
# Theme: Cerulean (bootswatch)

windowID="$(xprop -root '\t$0' _NET_ACTIVE_WINDOW | cut -f 2)"
synaptic --parent-window-id "$windowID" --non-interactive -o Synaptic::closeZvt=true --hide-main-window --update-at-startup
synaptic --parent-window-id "$windowID" --non-interactive -o Synaptic::closeZvt=true --hide-main-window --dist-upgrade-mode